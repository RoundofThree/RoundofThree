# List of projects 

- [ ] Golang executable de-stripping
- [ ] Rust `math` library: combinatorics, derivatives, integration, equations, iterative methods...
- [ ] Rust optimisation library: network flow, path, linear optimisation...
- [ ] Game development using Rust game engine
- [ ] Python bytecode backend analyser and frontend patch editor
