## Introduction

[![Mail](https://img.shields.io/badge/-zhuoyingjiangli@gmail.com-c14438?style=flat&logo=Gmail&logoColor=white&link=mailto:zhuoyingjiangli@gmail.com)](mailto:zhuoyingjiangli@gmail.com)

[RoundofThree](https://roundofthree.github.io) is a Computer Science student who loves playing with new stuff.

---

#### Stats

![](https://github-readme-stats-rho-mocha.vercel.app/api?username=RoundofThree&show_icons=true&theme=radical&&count_private=true)

---

#### Tech stack

<code><img width="5%" src="https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/python/python-ar21.svg"></code>
<code><img width="5%" src="https://www.vectorlogo.zone/logos/golang/golang-icon.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/amazon_aws/amazon_aws-ar21.svg"></code>
<code><img width="5%" src="https://www.vectorlogo.zone/logos/ruby-lang/ruby-lang-icon.svg"></code>
<code><img width="5%" src="https://www.vectorlogo.zone/logos/vuejs/vuejs-icon.svg"></code>
<code><img width="10%" src="https://www.vectorlogo.zone/logos/scala-lang/scala-lang-ar21.svg"></code>
<code><img width="5%" src="https://www.vectorlogo.zone/logos/rust-lang/rust-lang-icon.svg"></code>

![](https://github-readme-stats.vercel.app/api/top-langs/?username=RoundofThree&layout=compact&hide=html,tcl&langs_count=10&count_private=true)

<!-- ![](https://github-readme-stats-rho-mocha.vercel.app/api/wakatime?username=RoundofThree) --> 

#### Projects

- Targeted and targeted backdoor poisoning attacks against Drebin under problem-space derived constraints: request for access
- [Nyxeon](https://github.com/RoundofThree/nyxeon) (Golang Gin + Vue 3 first project) 
- King's College London Chinese Alumni Association website CMS (Ruby on Rails)
- Online health monitoring and scheduling system (Ruby on Rails)
- King's College London Chinese Student Association WeChat miniprogram
- Bank scam bait web application (Ruby on Rails) 
- [Watermark removal (python-scripts)](https://github.com/RoundofThree/python-scripts) (Python OpenCV)
- [Automated baselining](https://github.com/RoundofThree/automated-baselining) (Batchfile)
- AirBnB market listing desktop application (Java, first year coursework)

#### Things I like

- Security research, pwn, reversing and forensics challenges
- Systems programming, operating systems
- Compilers
- Mathematical optimization and algorithms
- Competitive programming with C++
- ACG yyds 🤟
